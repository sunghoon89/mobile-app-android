package com.google.ads.interactivemedia.v3.samples.videoplayerapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment for displaying a playlist of video thumbnails that the user can select from to play.
 */
public class VideoListFragment extends Fragment {

    private OnVideoSelectedListener mSelectedCallback;
    LayoutInflater mInflater;
    ViewGroup mContainer;
    //public int currentTag = -1;

    /**
     * Listener called when the user selects a video from the list.
     * Container activity must implement this interface.
     */
    public interface OnVideoSelectedListener {
        public void onVideoSelected(VideoItem videoItem);
    }

    private OnVideoListFragmentResumedListener mResumeCallback;

    /**
     * Listener called when the video list fragment resumes.
     */
    public interface OnVideoListFragmentResumedListener {
        public void onVideoListFragmentResumed();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mSelectedCallback = (OnVideoSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement " + OnVideoSelectedListener.class.getName());
        }

        try {
            mResumeCallback = (OnVideoListFragmentResumedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement " + OnVideoListFragmentResumedListener.class.getName());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
/*
        mInflater = inflater;
        mContainer = container;
        View rootView = inflater.inflate(R.layout.fragment_video_list, container, false);

        final ListView listView = (ListView) rootView.findViewById(R.id.videoListView);
        VideoItemAdapter videoItemAdapter = new VideoItemAdapter(rootView.getContext(),
                R.layout.video_item, getVideoItems());
        listView.setAdapter(videoItemAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                if (mSelectedCallback != null) {
                    VideoItem selectedVideo = (VideoItem) listView.getItemAtPosition(position);

                    // If applicable, prompt the user to input a custom ad tag.
                    if (selectedVideo.getAdTagUrl().equals(getString(
                            R.string.custom_ad_tag_value))) {
                        getCustomAdTag(selectedVideo);
                    } else {
                        mSelectedCallback.onVideoSelected(selectedVideo);
                    }
                }
            }
        });

        return rootView;
*/

        mInflater = inflater;
        mContainer = container;
        View rootView = inflater.inflate(R.layout.fragment_video_list, container, false);
        //initUi(rootView);
        final ListView listView = (ListView) rootView.findViewById(R.id.videoListView);
        VideoItemAdapter videoItemAdapter = new VideoItemAdapter(rootView.getContext(),
                R.layout.video_item, getVideoItems());
        listView.setAdapter(videoItemAdapter);
        VideoItem selectedVideo = (VideoItem) listView.getItemAtPosition(7);
        getCustomAdTag(selectedVideo);

        //VideoItem selectedVideo = (VideoItem) listView.getItemAtPosition(currentTag);
        //mSelectedCallback.onVideoSelected(selectedVideo);
        //return rootView;
        return null;
    }
/*
    private void initUi(View rootView) {
        VideoPlayerWithAdPlayback mVideoPlayerWithAdPlayback = (VideoPlayerWithAdPlayback)
                rootView.findViewById(R.id.videoPlayerWithAdPlayback);
        View playButton = rootView.findViewById(R.id.playButton);
        View playPauseToggle = rootView.findViewById(R.id.videoContainer);
        ViewGroup companionAdSlot = (ViewGroup) rootView.findViewById(R.id.companionAdSlot);
        mVideoTitle = (TextView) rootView.findViewById(R.id.video_title);
        mVideoExampleLayout = (LinearLayout) rootView.findViewById(R.id.videoExampleLayout);

        final TextView logText = (TextView) rootView.findViewById(R.id.logText);
        final ScrollView logScroll = (ScrollView) rootView.findViewById(R.id.logScroll);

        // Provide an implementation of a logger so we can output SDK events to the UI.
        VideoPlayerController.Logger logger = new VideoPlayerController.Logger() {
            @Override
            public void log(String message) {
                Log.i("ImaExample", message);
                if (logText != null) {
                    logText.append(message);
                }
                if (logScroll != null) {
                    logScroll.post(new Runnable() {
                        @Override
                        public void run() {
                            logScroll.fullScroll(View.FOCUS_DOWN);
                        }
                    });
                }
            }
        };

        mVideoPlayerController = new VideoPlayerController(this.getActivity(),
                mVideoPlayerWithAdPlayback, playButton, playPauseToggle,
                getString(R.string.ad_ui_lang), companionAdSlot, logger);

        // If we've already selected a video, load it now.
        if (mVideoItem != null) {
            loadVideo(mVideoItem);
        }
    }
*/
    private void getCustomAdTag(VideoItem originalVideoItem) {
        View dialogueView = mInflater.inflate(R.layout.custom_ad_tag, mContainer, false);
        final EditText txtUrl = (EditText) dialogueView.findViewById(R.id.customTag);
        txtUrl.setHint("VAST ad tag URL");
        final CheckBox isVmap = (CheckBox) dialogueView.findViewById(R.id.isVmap);
        final VideoItem videoItem = originalVideoItem;

        String customAdTagUrl = "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/1249652/TEST_OSV_Desktop_DR_DR_640x480&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&url=[referrer_url]&description_url=[description_url]&correlator=[timestamp]";
        VideoItem customAdTagVideoItem = new VideoItem(videoItem.getVideoUrl(),
                videoItem.getTitle(), customAdTagUrl, videoItem.getImageResource(),
                isVmap.isChecked());
        //VideoItem customAdTagVideoItem = new VideoItem(videoItem.getVideoUrl(),
        //         customAdTagUrl,
        //          isVmap.isChecked());
        if (mSelectedCallback != null) {
            mSelectedCallback.onVideoSelected(customAdTagVideoItem);
        }
        /*
        new AlertDialog.Builder(this.getActivity())
                .setTitle("Custom VAST Ad Tag URL")
                .setView(dialogueView)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String customAdTagUrl = txtUrl.getText().toString();
                        VideoItem customAdTagVideoItem = new VideoItem(videoItem.getVideoUrl(),
                                videoItem.getTitle(), customAdTagUrl, videoItem.getImageResource(),
                                isVmap.isChecked());

                        if (mSelectedCallback != null) {
                            mSelectedCallback.onVideoSelected(customAdTagVideoItem);
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                })
                .show();
        */
    }

    private List<VideoItem> getVideoItems() {
        final List<VideoItem> videoItems = new ArrayList<VideoItem>();

        // Iterate through the videos' metadata and add video items to list.
        for (int i = 0; i < VideoMetadata.APP_VIDEOS.size(); i++) {
            VideoMetadata videoMetadata = VideoMetadata.APP_VIDEOS.get(i);
            videoItems.add(new VideoItem(videoMetadata.videoUrl, videoMetadata.title,
                    videoMetadata.adTagUrl, videoMetadata.thumbnail, videoMetadata.isVmap));

            //videoItems.add(new VideoItem(videoMetadata.videoUrl,
            //        videoMetadata.adTagUrl,  videoMetadata.isVmap));
        }

        return videoItems;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mResumeCallback != null) {
            mResumeCallback.onVideoListFragmentResumed();
        }
    }
}
